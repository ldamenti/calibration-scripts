#!/bin/bash 

declare -i fd=0
####################################################
# Create a directory where store the results:
#if ! test -d Results/OscillationResults; then
#	mkdir Results/OscillationResults
#fi
# Change the config file to make it save the results in the directory previously created:
#sed -i '299 s# *<Setting name="DataOutputDir">           #    <Setting name="DataOutputDir">           /Results/OscillationResults #' CMSIT_RD53B.xml
####################################################

# Change the name of the output directory: 
mv Results Results_Temp

if  test -d OscillationResults; then
	rm -r OscillationResults
fi
 
for ((fd=0; fd<61; fd+=5));do 
	# Set the Delay and other options:
	sed -i CMSIT_RD53B.xml -e 's# *CAL_EDGE_FINE_DELAY *= *".*"#              CAL_EDGE_FINE_DELAY     =      "'$fd'"#g' 
	echo $fd
	sed -i 's/ *\(<Setting name="nEvents">\) *.* /    \1             100 /' CMSIT_RD53B.xml  
	sed -i 's/ *\(<Setting name="nEvtsBurst">\) *.* /    \1          100 /' CMSIT_RD53B.xml  
	sed -i 's/ *\(<Setting name="INJtype">\) *.* /    \1               1 /' CMSIT_RD53B.xml  
	sed -i 's/ *\(<Setting name="DisplayHisto">\) *.* /    \1          0 /' CMSIT_RD53B.xml  
	
	# Run the Scan:
	CMSITminiDAQ -f CMSIT_RD53B.xml -c scurve 
	
	# Change the name of the .root file:
	mv Results/*.txt Results/ConfigFileSCurve$fd.txt
	mv Results/*.xml Results/ConfigFileSCurve$fd.xml
	mv Results/*.root Results/SCurve$fd.root
	
	if ! test -d Results/ConfigFiles; then
		mkdir Results/ConfigFiles
	fi
	mv Results/ConfigFileSCurve$fd.txt Results/ConfigFiles/
	mv Results/ConfigFileSCurve$fd.xml Results/ConfigFiles/
	
	if ! test -d Results/RootFiles; then
		mkdir Results/RootFiles
	fi
	mv Results/SCurve$fd.root Results/RootFiles/
	
done

mv Results OscillationResults
mv Results_Temp Results


