#include <TFile.h>
#include <TTree.h>
#include <TDirectoryFile.h>
#include <TH1F.h>
#include <TSystem.h>
#include <TGraphErrors.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TLegend.h>
#include <TF1.h>
#include <TH2F.h>

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <cmath>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <stdlib.h>

int OscillationVisualizer(){

	// Set the Fine Delay value and run an scurve scan
	system("bash SCurveRun.sh");

	// Define three arrays, one for the Fine Delay, one for the Threshold and one for the thr dispersion (a triplet for each cip):
	// (i.e. dual modules -> two sets of arrays; quad modules -> four sets of arrays)
	std::vector< double > FineDelay_Chip1;
	std::vector< double > Threhsold_Chip1;
	std::vector< double > ThrDisp_Chip1;
	
	std::vector< double > FineDelay_Chip2;
	std::vector< double > Threhsold_Chip2;
	std::vector< double > ThrDisp_Chip2;
	
	for(int n=4; n<=5; n++){
	
		//Loop over all the possible Fine Delay values: 
		for (int i=0; i<61; i+=5){
			
			// Recover the value of the threshold
			TObject* anObject;
	    		double mean = 0;
	    		double rms = 0;	
	    		double N = 0;
	    		
	    		std::string dirName = Form("Detector;1/Board_0;1/OpticalGroup_0;1/Hybrid_0;1/Chip_%d;1",n);
			std::string histName = Form("D_B(0)_O(0)_H(0)_Threshold1D_Chip(%d);1",n);
			
			TFile *file0 = TFile::Open(Form("OscillationResults/RootFiles/SCurve%d.root",i));
			// Comment the previous line and uncomment the following to analize data stored in OscillationData
			// [In the latter case, remember to comment line 25 (system("bash SCurveRun.sh");)]
			//TFile *file0 = TFile::Open(Form("OscillationData/Run1/RootFiles/SCurve%d.root",i));
			
			anObject = file0->Get(dirName.c_str());

			if ((anObject) && (std::string(anObject->ClassName()) == "TDirectoryFile")) {

				TDirectoryFile* myDir = (TDirectoryFile*) anObject;
				
				TObject* obj1 = myDir->Get("Board_0;1");
				TDirectoryFile* mysubDir1 = (TDirectoryFile*) obj1;
				
				TObject* obj2 = mysubDir1->Get("OpticalGroup_0;1");
				TDirectoryFile* mysubDir2 = (TDirectoryFile*) obj2;
				
				TObject* obj3 = mysubDir2->Get("Hybrid_0;1");
				TDirectoryFile* mysubDir3 = (TDirectoryFile*) obj3;
				
				TObject* obj4 = mysubDir3->Get(Form("Chip_%d;1",n));
				TDirectoryFile* mysubDir4 = (TDirectoryFile*) obj4;
				
				TObject* anObject = mysubDir4->Get(histName.c_str());
				
				if ((anObject) && (std::string(anObject->ClassName())=="TCanvas")) {
					TCanvas* myCanvas = (TCanvas*)anObject;
					myCanvas->GetListOfPrimitives();
					TH1F* myHisto = (TH1F*)myCanvas->GetPrimitive(Form("D_B(0)_O(0)_H(0)_Threshold1D_Chip(%d)",n));
					mean = myHisto->GetMean();
					rms = myHisto->GetRMS();
					N = myHisto->GetEntries();
				} else {
					std::cerr << "ERROR: could not find a TCanvas with name " << histName << std::endl;
				}
			} else {
				std::cerr << "ERROR: could not find a TDirectoryFile with name " << dirName << std::endl;
			}

			int ConversionFactor = 5; // e/Vcal
			double Thr = mean*ConversionFactor;
			double ThrD = (rms/sqrt(N))*ConversionFactor;
			    
			// Add the value of the Fine Delay and the Corresponding Threhsold to dedicated arrays:
			if (n==4){
				FineDelay_Chip1.push_back(i);
				Threhsold_Chip1.push_back(Thr);
				ThrDisp_Chip1.push_back(ThrD);
			}
			else{
				FineDelay_Chip2.push_back(i);
				Threhsold_Chip2.push_back(Thr);
				ThrDisp_Chip2.push_back(ThrD);
			}
		}
	
	
	}
		
	
	// Define plots which show the behavior of the Threshold with the Fine Delay:
	TCanvas* can_chip1 = new TCanvas("Can_chip1","Canvas_chip1");
	TCanvas* can_chip2 = new TCanvas("Can_chip2","Canvas_chip2");
	
	TGraphErrors* plot_chip1 = new TGraphErrors((int)FineDelay_Chip1.size(), &(FineDelay_Chip1[0]), &(Threhsold_Chip1[0]), 0, &(ThrDisp_Chip1[0]));
	TGraphErrors* plot_chip2 = new TGraphErrors((int)FineDelay_Chip2.size(), &(FineDelay_Chip2[0]), &(Threhsold_Chip2[0]), 0, &(ThrDisp_Chip2[0]));
	
	can_chip1->cd();
	plot_chip1->Draw();
	plot_chip1->SetTitle("Threshold vs Fine Delay (chip 4)");
	plot_chip1->GetXaxis()->SetTitle("Fine Delay [0.78ns]");
	plot_chip1->GetYaxis()->SetTitle("Threhsold [e]");
	
	
	can_chip2->cd();
	plot_chip2->Draw();
	plot_chip2->SetTitle("Threshold vs Fine Delay (chip 5)");
	plot_chip2->GetXaxis()->SetTitle("Fine Delay [0.78ns]");
	plot_chip2->GetYaxis()->SetTitle("Threhsold [e]");
	
		
    
    return 0;
}


 
