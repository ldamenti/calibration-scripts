#include <TFile.h>
#include <TTree.h>
#include <TDirectoryFile.h>
#include <TH1F.h>
#include <TSystem.h>
#include <TGraphErrors.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TLegend.h>
#include <TF1.h>
#include <TH2F.h>

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>

void FindOFD_SCC(void)
{
    	int OptimalFD;
    	
	// Recover the tornado 
	TObject* anObject;
	TH2F* myHisto;
	double mean = 0;
	double rms = 0;	
		    		
	std::string dirName = "Detector;1/Board_0;1/OpticalGroup_0;1/Hybrid_0;1/Chip_15;1";
        std::string histName = "D_B(0)_O(0)_H(0)_GenericDacDacScanScan_Chip(15);1";     
			
	TFile *file0 = TFile::Open("Results/TornadoDac.root");
	//Test:
	//TFile *file0 = TFile::Open("Results/Run000119_GenericDacDac.root");
			
	anObject = file0->Get(dirName.c_str());

	if ((anObject) && (std::string(anObject->ClassName()) == "TDirectoryFile")) {

		TDirectoryFile* myDir = (TDirectoryFile*) anObject;
				
		TObject* obj1 = myDir->Get("Board_0;1");
		TDirectoryFile* mysubDir1 = (TDirectoryFile*) obj1;
				
		TObject* obj2 = mysubDir1->Get("OpticalGroup_0;1");
		TDirectoryFile* mysubDir2 = (TDirectoryFile*) obj2;
				
		TObject* obj3 = mysubDir2->Get("Hybrid_0;1");
		TDirectoryFile* mysubDir3 = (TDirectoryFile*) obj3;
				
		TObject* obj4 = mysubDir3->Get("Chip_15;1");
		TDirectoryFile* mysubDir4 = (TDirectoryFile*) obj4;
				
		TObject* anObject = mysubDir4->Get(histName.c_str());
				
		if ((anObject) && (std::string(anObject->ClassName())=="TCanvas")) {
			TCanvas* myCanvas = (TCanvas*)anObject;
			myCanvas->GetListOfPrimitives(); //->ls();
			TH2F* myHisto = (TH2F*)myCanvas->GetPrimitive("D_B(0)_O(0)_H(0)_GenericDacDacScanScan_Chip(15)"); 	
				
			// Projection:
			TH1D* projection = new TH1D;
			projection = myHisto->ProjectionX("Projection", 6, 6);
				
			// Create the model: 
		    	TF1 *cdfunc  = new TF1("cdf","ROOT::Math::normal_cdf(x, [0],[1])",5,25);
		    	cdfunc->SetParameter(0,5);
		    	cdfunc->SetParameter(1,15);
		    	cdfunc->SetParNames("Sigma");
		    	cdfunc->SetParNames("Mu");
		    		
		    	projection->Fit("cdf","q");

		    	int OptimalX = (int)projection->GetFunction("cdf")->GetParameter(1);

		    	OptimalFD = OptimalX;    
				  
		} else {
			std::cerr << "ERROR: could not find a TCanvas with name " << histName << std::endl;
			}
	} else {
		std::cerr << "ERROR: could not find a TDirectoryFile with name " << dirName << std::endl;
	}

	
    
    	std::cout<<OptimalFD<<std::endl;
    	

}




