#!/bin/bash 

sed -i 's/ *\(<Setting name="DisplayHisto">\) *.* /    \1          0 /' CMSIT_RD53B.xml

##########################################################

# First of all, its important to find and set the Optimal Fine Delay:
sed -i 's/ *\(<Setting name="nEvents">\) *.* /    \1             100 /' CMSIT_RD53B.xml
sed -i 's/ *\(<Setting name="nEvtsBurst">\) *.* /    \1          100 /' CMSIT_RD53B.xml
sed -i 's/ *\(<Setting name="nTRIGxEvent">\) *.* /    \1           1 /' CMSIT_RD53B.xml
sed -i 's/ *\(<Setting name="DoOnlyNGroups">\) *.* /    \1         1 /' CMSIT_RD53B.xml
sed -i 's/ *\(<Setting name="InjLatency">\) *.* /    \1           33 /' CMSIT_RD53B.xml
# Set the DacDac scan options so to have a Low Res TornadoPlot:
sed -i 's/ *\(<Setting name="RegNameDAC1">\) *.* /    \1         CAL_EDGE_FINE_DELAY /' CMSIT_RD53B.xml
sed -i 's/ *\(<Setting name="StartValueDAC1">\) *.* /    \1        0 /' CMSIT_RD53B.xml
sed -i 's/ *\(<Setting name="StopValueDAC1">\) *.* /    \1        30 /' CMSIT_RD53B.xml
sed -i 's/ *\(<Setting name="StepDAC1">\) *.* /    \1              1 /' CMSIT_RD53B.xml
sed -i 's/ *\(<Setting name="RegNameDAC2">\) *.* /    \1   VCAL_HIGH /' CMSIT_RD53B.xml
sed -i 's/ *\(<Setting name="StartValueDAC2">\) *.* /    \1     2900 /' CMSIT_RD53B.xml
sed -i 's/ *\(<Setting name="StopValueDAC2">\) *.* /    \1      3000 /' CMSIT_RD53B.xml
sed -i 's/ *\(<Setting name="StepDAC2">\) *.* /    \1             10 /' CMSIT_RD53B.xml

mv Results Results_temp

CMSITminiDAQ -f CMSIT_RD53B.xml -c gendacdac

mv Results/*.root Results/TornadoDac.root
mv Results/TornadoDac.root Results_temp
rm -r Results

mv Results_temp Results


Output=$(root -l FindOFD_SCC.cc -q)
String_Delay=${Output:30:2}

sed -i CMSIT_RD53B.xml -e 's# *CAL_EDGE_FINE_DELAY *= *".*"#              CAL_EDGE_FINE_DELAY     =      "'$String_Delay'"#g'

#rm Results/TornadoDac.root

##########################################################

# Set a High value of the Threshodl (i.e. 3000e):
sed -i 's/ *\(<Setting name="nTRIGxEvent">\) *.* /    \1          10 /' CMSIT_RD53B.xml
sed -i 's/ *\(<Setting name="INJtype">\) *.* /    \1               1 /' CMSIT_RD53B.xml
sed -i 's/ *\(<Setting name="InjLatency">\) *.* /    \1           32 /' CMSIT_RD53B.xml
sed -i 's/ *\(<Setting name="DoOnlyNGroups">\) *.* /    \1         0 /' CMSIT_RD53B.xml
sed -i 's/ *\(<Setting name="TargetThr">\) *.* /    \1          3000 /' CMSIT_RD53B.xml

CMSITminiDAQ -f CMSIT_RD53B.xml -c thradj

##########################################################

# Equalize the Threshold:
sed -i 's/ *\(<Setting name="nEvents">\) *.* /    \1             500 /' CMSIT_RD53B.xml
sed -i 's/ *\(<Setting name="nEvtsBurst">\) *.* /    \1          500 /' CMSIT_RD53B.xml
sed -i 's/ *\(<Setting name="nTRIGxEvent">\) *.* /    \1          10 /' CMSIT_RD53B.xml
sed -i 's/ *\(<Setting name="DoNSteps">\) *.* /    \1              0 /' CMSIT_RD53B.xml

CMSITminiDAQ -f CMSIT_RD53B.xml -c threqu

##########################################################

# Set the threshold to a Low value (i.e 1000e):
sed -i 's/ *\(<Setting name="nEvents">\) *.* /    \1             100 /' CMSIT_RD53B.xml
sed -i 's/ *\(<Setting name="nEvtsBurst">\) *.* /    \1          100 /' CMSIT_RD53B.xml
sed -i 's/ *\(<Setting name="nTRIGxEvent">\) *.* /    \1          10 /' CMSIT_RD53B.xml
sed -i 's/ *\(<Setting name="TargetThr">\) *.* /    \1          1000 /' CMSIT_RD53B.xml

CMSITminiDAQ -f CMSIT_RD53B.xml -c thradj

##########################################################

# Run a noise scan and a pixealive scan in order to find and mask stuck and noisy pixels:
sed -i 's/ *\(<Setting name="nEvents">\) *.* /    \1             100 /' CMSIT_RD53B.xml
sed -i 's/ *\(<Setting name="nEvtsBurst">\) *.* /    \1          100 /' CMSIT_RD53B.xml
sed -i 's/ *\(<Setting name="nTRIGxEvent">\) *.* /    \1          10 /' CMSIT_RD53B.xml
sed -i 's/ *\(<Setting name="OccPerPixel">\) *.* /    \1        2e-5 /' CMSIT_RD53B.xml
sed -i 's/ *\(<Setting name="UnstuckPixels">\) *.* /    \1         0 /' CMSIT_RD53B.xml

CMSITminiDAQ -f CMSIT_RD53B.xml -c pixelalive

sed -i 's/ *\(<Setting name="nEvents">\) *.* /    \1         1000000 /' CMSIT_RD53B.xml
sed -i 's/ *\(<Setting name="nEvtsBurst">\) *.* /    \1        10000 /' CMSIT_RD53B.xml
sed -i 's/ *\(<Setting name="INJtype">\) *.* /    \1               0 /' CMSIT_RD53B.xml

CMSITminiDAQ -f CMSIT_RD53B.xml -c noise

##########################################################

# Verify the tuning with an SCurve (not saved in Results):
sed -i 's/ *\(<Setting name="nEvents">\) *.* /    \1             100 /' CMSIT_RD53B.xml
sed -i 's/ *\(<Setting name="nEvtsBurst">\) *.* /    \1          100 /' CMSIT_RD53B.xml
sed -i 's/ *\(<Setting name="INJtype">\) *.* /    \1               1 /' CMSIT_RD53B.xml
sed -i 's/ *\(<Setting name="DisplayHisto">\) *.* /    \1          1 /' CMSIT_RD53B.xml

CMSITminiDAQ -f CMSIT_RD53B.xml -c scurve

##########################################################









