#include <TFile.h>
#include <TTree.h>
#include <TDirectoryFile.h>
#include <TH1F.h>
#include <TSystem.h>
#include <TGraphErrors.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TLegend.h>
#include <TF1.h>
#include <TH2F.h>

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <cmath>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <stdlib.h>

int OscillationVisualizer_SCC(){

	// Set the Fine Delay value and run an scurve scan
	system("bash SCurveRun.sh");

	// Define three arrays, one for the Fine Delay, one for the Threshold and one for the thr dispersion:
	std::vector< double > FineDelay;
	std::vector< double > Threhsold;
	std::vector< double > ThrDisp;
 
 
	//Loop over all the possible Fine Delay values: 
	for (int i=0; i<61; i+=5){
		
		// Recover the value of the threshold
		TObject* anObject;
    		double mean = 0;
    		double rms = 0;	
    		double N = 0;
    		
    		std::string dirName = "Detector;1/Board_0;1/OpticalGroup_0;1/Hybrid_0;1/Chip_15;1";
		std::string histName = "D_B(0)_O(0)_H(0)_Threshold1D_Chip(15);1";
		TFile *file0 = TFile::Open(Form("OscillationResults/RootFiles/SCurve%d.root",i));
		// Comment the previous line and uncomment the following to analize data stored in OscillationData
		// [In the latter case, remember to comment line 25 (system("bash SCurveRun.sh");)]
		//TFile *file0 = TFile::Open(Form("OscillationData/Run1/RootFiles/SCurve%d.root",i));
		
		anObject = file0->Get(dirName.c_str());

		if ((anObject) && (std::string(anObject->ClassName()) == "TDirectoryFile")) {

			TDirectoryFile* myDir = (TDirectoryFile*) anObject;
			
			TObject* obj1 = myDir->Get("Board_0;1");
			TDirectoryFile* mysubDir1 = (TDirectoryFile*) obj1;
			
			TObject* obj2 = mysubDir1->Get("OpticalGroup_0;1");
			TDirectoryFile* mysubDir2 = (TDirectoryFile*) obj2;
			
			TObject* obj3 = mysubDir2->Get("Hybrid_0;1");
			TDirectoryFile* mysubDir3 = (TDirectoryFile*) obj3;
			
			TObject* obj4 = mysubDir3->Get("Chip_15;1");
			TDirectoryFile* mysubDir4 = (TDirectoryFile*) obj4;
			
			TObject* anObject = mysubDir4->Get(histName.c_str());
			
			if ((anObject) && (std::string(anObject->ClassName())=="TCanvas")) {
				TCanvas* myCanvas = (TCanvas*)anObject;
				myCanvas->GetListOfPrimitives();
				TH1F* myHisto = (TH1F*)myCanvas->GetPrimitive("D_B(0)_O(0)_H(0)_Threshold1D_Chip(15)");
				mean = myHisto->GetMean();
			        rms = myHisto->GetRMS();
			        N = myHisto->GetEntries();
			} else {
			        std::cerr << "ERROR: could not find a TCanvas with name " << histName << std::endl;
			}
		} else {
			std::cerr << "ERROR: could not find a TDirectoryFile with name " << dirName << std::endl;
		}

	        int ConversionFactor = 5; // e/Vcal
		double Thr = mean*ConversionFactor;
		double ThrD = (rms/sqrt(N))*ConversionFactor;
		    
		// Add the value of the Fine Delay and the Corresponding Threhsold to dedicated arrays:
		FineDelay.push_back(i);
		Threhsold.push_back(Thr);
		ThrDisp.push_back(ThrD);
	}
	
	// Define a plot which shows the behavior of the Threshold with the Fine Delay:
	TCanvas* can = new TCanvas("Can","Canvas");
	
	TGraphErrors* plot = new TGraphErrors((int)FineDelay.size(), &(FineDelay[0]), &(Threhsold[0]), 0, &(ThrDisp[0]));
	
	plot->SetTitle("Threshold vs Fine Delay");
	plot->GetXaxis()->SetTitle("Fine Delay [0.78ns]");
	plot->GetYaxis()->SetTitle("Threhsold [e]");
	
	plot->Draw();
	

    
    return 0;
}


 
