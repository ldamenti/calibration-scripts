The scripts loaded in this repository could be used to tune the threshold of SCCs or dual modules to 1000e, and to study the oscillation of the threshold with respect to Fine Delay. 

NOTE: Before running any of the scripts you must be sure that the value of the GDAC register is not reported in the .xml file (i.e. not DAC_GDAC_*_LIN = "450" but DAC_GDAC_*_LIN = "").

##### Threshold Tuning Procedure #####
To tune the threshold to 1000e the script ThrTuningProcedure.sh needs to be run. 
It adjust and equalize the threshold to 3000e before adjust it to 1000e. 
Stuck and noisy pixels are masked my running, respectively, a pixelalive scan and a noise scan after the threshold tuning. 
All the threshold tuning procedure is run after setting the optimal fine delay for the calibration injections. 
The value of the Optimal Fine Delay is found by FindOFD.cc (it is run by ThrTuningProcedure.sh, so it does not need to be run separately). 
[Brief description of how FindOFD.cc finds the Optimal Fine Delay -> The Optimal value for the Fine Delay register (named OFD) corresponds to the point of minimum of the tornado plot. So, a possible procedure could be the following: run a gendadac scan in order to obtain the tornado plot, cut the lower part of the tornado and project it along the x-axis. By doing so, a distribution of Fine Delay is obtained. The mean value of the latter distribution is the OFD. However, the gendacdac scan is really time consuming (it could take up to 35 minutes to end), so the following alternative procedure has been implemented, in order to save some time. A gendacdac is performed considering a small range of really high Vcal value (i.e. from 2000 to 2100 Vcal) and scanning the Fine Delay from 0 to 30. By doing so, the projection along the x-axis is a step function which can be fitted in order to obtain the value of the FD that corresponds to 50% occupancy, named FD_fitted. The Optimal Fine Delay is FD_Fitted increased by some units in order to get as close as possible to the minimum of the Tornado Plot.]

NOTE: Some known errors could appear when running the tuning script:
1) If, after the tuning procedure, the value of the tuned threshold is always higher than 1000e, try to descrease the lower limit of the GDAC range which can be set in the .xml file. 
2) If FIFO errors appear during threshold adjustment at 1000e, try to modify the LDAC value in order to increase the GDAC value corresponding to 1000e for the given chip

NOTE: If the threshold tunig procedure fails, it's important to follow the following steps before trying another tuning:
1) Use new .txt file/files
2) Set in the .xml high GDAC values (i.e. DAC_GDAC_*_LIN = "450") and run a pixelalive to verify that the SCC/module is working fine. 
3) Remove the GDAC values setted in the previous step and run ThrTuningProcedure.sh again 


#####   Threshold Oscillation    #####
The oscillation of the threshold with respect to the Fine Delay can be obtained by running OscillationVisualizer.cc.
It scans the Fine Delay register and, for each value of it, the script runs an scurve scan (using SCurveRun.sh) from which the value of the threshold is obtained. 
Note that the oscillation of each chip is a mean oscillation of all injected pixels.



